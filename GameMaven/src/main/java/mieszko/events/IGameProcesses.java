package mieszko.events;

public interface IGameProcesses {
	
	public void processGame(GameEvent gameEvent);
}
